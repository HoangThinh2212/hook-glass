import React from "react";

export default function Model({ glass }) {
  const { url, desc, name, price } = glass;
  return (
    <div className="card mx-auto border-muted">
      <img
        src="./glassesImage/model.jpg"
        className="card-img-top"
        alt="model"
      />
      <img
        src={url}
        className="position-absolute mx-auto"
        style={{
          left: 4,
          right: 4,
          top: 130,
          width: 240,
        }}
        alt=""
      />
      <div className="card-body text-start bg-info">
        <h5 className="card-title text-white">{name}</h5>
        <p className="card-text font-italic">Description: {desc}</p>
        <h3 className="card-text font-weight-bold">Price: {price}</h3>
      </div>
    </div>
  );
}
