import { useState } from "react";
import "./App.css";
import GlassList from "./Glass/GlassList";
import Model from "./Glass/Model";
import dataGlass from "./dataGlasses.json";

const data = {
  id: "",
  price: "",
  name: "",
  url: "",
  desc: "",
};

function App() {
  const [glass, setGlass] = useState(data);
  return (
    <div className="App">
      <div className="container h-100">
        <div className="bg-info display-4 p-3 mb-2 text-warning">Hook Glasses</div>
        <div className="row align-items-center">
          <div className="col-6">
            <Model glass={glass} />
          </div>
          <div className="col-6">
            <GlassList dataGlass={dataGlass} setGlass={setGlass} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
